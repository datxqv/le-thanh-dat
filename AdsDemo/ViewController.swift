//
//  ViewController.swift
//  AdsDemo
//
//  Created by Dat Le on 2020/08/14.
//  Copyright © 2020 Dat Le. All rights reserved.
//

import UIKit
import AdSupport

class ViewController: UIViewController {
    
    @IBOutlet var idfLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func getIdf(_ sender: Any) {
        if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
            
            idfLabel.text = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        } else {
            print("Idf string is not avaiable")
        }
    }
}

